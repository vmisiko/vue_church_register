export default {
  isAuthenticated: (state) => state.token !== null,
  getSession: (state) => state.sessionData,
  getloginErrors: (state) => state.loginErrors,
  getTokenExpiryStatus: (state) => state.tokenExpired,
  getSessionRefreshed: (state) => state.sessionRefreshed,
  getErrors: state => state.errors,
  getDrawerStatus: state => state.drawerStatus,
};
