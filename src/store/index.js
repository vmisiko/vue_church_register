import Vue from 'vue';
import Vuex from 'vuex';
import globalStore from './global';

Vue.use(Vuex);
const createStore = new Vuex.Store(globalStore);

export default createStore;
